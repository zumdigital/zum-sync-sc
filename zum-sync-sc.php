<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

//echo dirname( __FILE__ );

require_once(dirname( __FILE__ ).'/../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../wp-load.php');

require_once 'model/product.php';
require_once 'controller/solidcommerceapi.php';
/*
* Plugin Name: ZumDigital Sync SolidCommerce Custom
* Description: This plugin works to sync data from Solid Commerce to Worpress
* Version: 1.0
* Author: DiegoCardona
* Email: diego0123@gmail.com
* Autor Uri: http://thedeveloper.co
*/

/* helpers */
function zum_sync_sc_get_url(){
	$plugin_dir = plugin_dir_path( __FILE__ );
	return plugin_dir_path( __FILE__ );
}

function zum_sync_sc_js_include()
{
    wp_enqueue_script('jquery');
    wp_register_style( 'bootstrap.css', WP_PLUGIN_URL. '/zum-sync-sc/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap.css' );
    wp_register_style( 'zum-sync-sc.css', WP_PLUGIN_URL. '/zum-sync-sc/css/plugin.css' );
    wp_enqueue_style( 'zum-sync-sc.css' );
    //wp_enqueue_script('jquery.js', WP_PLUGIN_URL. '/zum-sync-sc/js/jquery.min.js' );
    wp_enqueue_script('bootstrap.js', WP_PLUGIN_URL. '/zum-sync-sc/js/bootstrap.min.js' );
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    //wp_enqueue_script('vue.js', WP_PLUGIN_URL. '/zum-sync-sc/js/vue.min.js' );
    wp_enqueue_script('vue.js', WP_PLUGIN_URL. '/zum-sync-sc/js/vue.js' );
    wp_enqueue_script('axios.js', WP_PLUGIN_URL. '/zum-sync-sc/js/axios.min.js' );
    //wp_enqueue_script('script.js?time='.date("YmdHis"), WP_PLUGIN_URL. '/zum-sync-sc/js/script.js' );
    wp_enqueue_media();
}

add_action( 'admin_enqueue_scripts', 'zum_sync_sc_js_include' );

add_action( 'admin_menu', 'zum_sync_sc_custom_admin_menu' );
//add_menu_page( 'Test Plugin Page', 'Test Plugin', 'manage_options', 'zum-sync-sc-plugin', 'zum_sync_sc_main_page' );

 
function zum_sync_sc_custom_admin_menu() {
  //add_options_page
  // https://wordpress.org/support/article/roles-and-capabilities/#contributor
    add_menu_page(
        'SOLID COMMERCE SYNC',
        'SOLID COMMERCE SYNC',
        'edit_posts',
        'zum-sync-sc-plugin',
        'zum_sync_sc_main_page'
    );
    
}

function zum_sync_sc_main_page() {
	define('ZUM_SYNC_SC_PLUGUIN_NAME','zum-sync-sc');
	include_once zum_sync_sc_get_url() .'view/main_page.php';    
}

register_activation_hook( __FILE__, 'zum_sync_sc_create_db' );
function zum_sync_sc_create_db() {
    global $wpdb;
    $version = get_option( 'zum_sync_sc_version', '1.0' );
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'zum_sync_sc_fitments';

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        product_sku VARCHAR(500) NOT NULL,
        product_name  VARCHAR(500) NOT NULL,
        post_id  VARCHAR(500) NOT NULL,
        status  VARCHAR(20) DEFAULT 'Pending',
        last_updated_date  DATETIME NOT NULL,
        condition_description VARCHAR(500),
        image_notes VARCHAR(500),
        condition_general VARCHAR(500),
        model VARCHAR(500),
        features VARCHAR(500),
        primary_color VARCHAR(500),
        surface_finish VARCHAR(500),
        color_code VARCHAR(500),
        brand VARCHAR(500),
        model_year VARCHAR(500),
        manufacturer_part_number VARCHAR(500),
        other_part_number VARCHAR(500),
        interchange_part_number VARCHAR(500),
        placement_on_vehicle VARCHAR(500),
        donator VARCHAR(500),
        vin_number VARCHAR(500),
        return_general VARCHAR(500),
        oem VARCHAR(500),
        UNIQUE KEY id (id)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    $pluginlog = plugin_dir_path(__FILE__).'debug.log';
    $message = $wpdb->last_error.PHP_EOL;
    $message .= $sql.PHP_EOL;
    error_log($message, 3, $pluginlog);

/*



condition_description VARCHAR(500),
        image_notes VARCHAR(500),
        condition VARCHAR(500),
        model VARCHAR(500),
        features VARCHAR(500),
        primary_color VARCHAR(500),
        surface_finish VARCHAR(500),
        color_code VARCHAR(500),
        brand VARCHAR(500),
        model_year VARCHAR(500),
        manufacturer_part_number VARCHAR(500),
        other_part_number VARCHAR(500),
        interchange_part_number VARCHAR(500),
        placement_on_vehicle VARCHAR(500),
        donator VARCHAR(500),
        vin_number VARCHAR(500),
        return VARCHAR(500),
        oem VARCHAR(500),

*/
    // $table_name2 = $wpdb->prefix . 'zum_sync_sc_compatibilities';

    // $sql2 = "CREATE TABLE $table_name (
    //     id mediumint(9) NOT NULL AUTO_INCREMENT,
    //     id_fitment mediumint(9) NOT NULL,
    //     year VARCHAR(10) NOT NULL,
    //     make  VARCHAR(500) NOT NULL,
    //     model  VARCHAR(500) NOT NULL,
    //     ttrim  VARCHAR(1000) NOT NULL,
    //     engine  VARCHAR(1000) NOT NULL,
    //     UNIQUE KEY id (id)
    // ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";

    // require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    // dbDelta( $sql2 );
    

    //drop table `wp_zum_sync_sc_fitments`;
    //drop table `wp_zum_sync_sc_fitment_element`;
}

register_activation_hook( __FILE__, 'zum_sync_sc_create_db_2' );
function zum_sync_sc_create_db_2() {
    global $wpdb;
    $version = get_option( 'zum_sync_sc_version', '1.0' );
    $charset_collate = $wpdb->get_charset_collate();
    
    $table_name2 = $wpdb->prefix . 'zum_sync_sc_compatibilities';

    $sql2 = "CREATE TABLE $table_name2 (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        id_fitment mediumint(9),
        year VARCHAR(10),
        make  VARCHAR(500),
        model  VARCHAR(500),
        ttrim  VARCHAR(1000),
        engine  VARCHAR(1000),
        UNIQUE KEY id (id)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql2 );
    $pluginlog = plugin_dir_path(__FILE__).'debug.log';
    $message = $wpdb->last_error.PHP_EOL;
    $message .= $sql2.PHP_EOL;
    error_log($message, 3, $pluginlog);

}

function zum_sync_sc_get_all()
{
    return get_products();
}
add_action('init', 'zum_sync_sc_get_all');


function zum_sync_sc_js_include_front()
{
    
    wp_enqueue_script('vue.js', WP_PLUGIN_URL. '/zum-sync-sc/js/vue.js' );
    wp_enqueue_script('axios.js', WP_PLUGIN_URL. '/zum-sync-sc/js/axios.min.js' );
    wp_enqueue_script('zum_script.js', WP_PLUGIN_URL. '/zum-sync-sc/js/script.js?time='.date("YmdHis") );
    
}

add_action( 'wp_enqueue_scripts', 'zum_sync_sc_js_include_front' );

function zum_sync_sc_fitments($post_id) {
    ?>

<style>
/*the container must be positioned relative:*/
.custom-select {
  position: relative;
  font-family: Arial;
}
.custom-select select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: #f4f5fb;
  border-radius: 5px;
  border: 1px solid #e0e4f6;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 11px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #888 transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #888 transparent;
  top: 7px
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #9fa4bb;
  padding: 2px 4px;
  /*border: 1px solid #e0e4f6;*/
  /*border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;*/
  cursor: pointer;
  user-select: none;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: #f4f5fb;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

.zum-container {
    width: 100%;
    border: 0px solid ;
    padding: 4px;
    margin: 4px;
}
.zum-row {
    text-align: center;
}
.zum-btn {
    display: inline-block;
    margin-right: 6px;
    
}
.zum-clear {
    clear: both;
}

.zum-table{
  table-layout:fixed;
}
.zum-td{width:1px;white-space:nowrap;}

.zum-loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #999999;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.zum-container4 {
    height: 10em;
    position: relative }
.zum-container4 p {
    margin: 0;
    background: yellow;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }


/*@media only screen and (min-width: 600px) {
    // For tablets: 
    .col-s-1 {width: 8.33%;}
    .col-s-2 {width: 16.66%;}
    .col-s-3 {width: 25%;}
    .col-s-4 {width: 33.33%;}
    .col-s-5 {width: 41.66%;}
    .col-s-6 {width: 50%;}
    .col-s-7 {width: 58.33%;}
    .col-s-8 {width: 66.66%;}
    .col-s-9 {width: 75%;}
    .col-s-10 {width: 83.33%;}
    .col-s-11 {width: 91.66%;}
    .col-s-12 {width: 100%;}
}*/
.zum-mobile {
  display: inline;
}
.zum-desktop{
  display: none;
}
@media (min-width: 768px) {
    /* For desktop: */
    .zum-mobile {
      display: none;
    }
    .zum-desktop{
      display: inline;
    }
}
</style>

<input type="hidden" id="zum-post-id" v-model='post_id' value="<?php echo get_the_ID() ?>">


<div class="zum-sync-sc-main">
<div class="prod-tabs-wrap" id="zum-sync-sc-info" style="display: none"> 

   <ul class="prod-tabs">
      <li data-prodtab-num="1" id="prod-desc-info" class="active">
         <a data-prodtab="#zum-prod-tab-info-1" href="#">Product attributes</a>
      </li>
   </ul>
   <div class="prod-tab-cont">
      <p data-prodtab-num="1" class="prod-tab-mob active prod-tab-mob-description" data-prodtab="#zum-prod-tab-info-1">Product attributes</p>
      
      <div class="" id="zum-prod-tab-info-1" >

        <div >
         <table class="shop_table woocommerce-checkout-review-order-table">
            <!--<thead>
               <tr>
                  <th class="product-name">Name</th>
                  <th class="product-name">Value</th>
               </tr>
            </thead>-->
            <tbody>
               <tr class="cart_item" v-for="element in attributes">
                <td class="zum_sync_prod_at_name" v-text="element.name"></td>
                <td class="zum_sync_prod_at_value" v-text="element.value"></td>   
               </tr>
            </tbody>
         </table>
       </div>
   </div>

  </div>
  
</div>

<div class="prod-tabs-wrap " id="zum-sync-sc-main"> 

   <ul class="prod-tabs">
      <li data-prodtab-num="1" id="prod-desc" class="active">
         <a data-prodtab="#zum-prod-tab-1" href="#">Compatibility</a>
      </li>
   </ul>
   <div class="prod-tab-cont">
      <p data-prodtab-num="1" class="prod-tab-mob active prod-tab-mob-description" data-prodtab="#zum-prod-tab-1">Compatibility</p>
      <div class="" id="zum-prod-tab-1">

        <div class="zum-element zum-loading zum-container">
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">
            <div class="zum-btn zum-container4">
              <div class="zum-loader"></div>
            </div>
          </div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
          <div class="zum-row">&nbsp;</div>
        </div>
      </div>
      <div class="zum-desktop" id="zum-prod-tab-1" v-if="true">

       
       
        <!-- https://www.w3schools.com/howto/tryit.asp?filename=tryhow_custom_select -->
        <div class='zum-element zum-container '>
            <div class='zum-row'>
                <div class='zum-btn'>
                  <select v-model="query.year">
                    <option value="">Select Year:</option>
                    <option 
                      v-for="year in parameters.year" 
                      :value="year.year" v-text="year.year">
                      </option>
                  </select>
                </div>
                <div class='zum-btn'>
                   <select v-model="query.make" v-on:change="changeFilter()">
                    <option value="">Select Make:</option>
                    <option v-if="query.year != ''"
                      v-for="make in parameters.make" 
                      :value="make.make" v-text="make.make">
                      </option>
                  </select>
                </div>
                <div class='zum-btn'>
                    <select v-model="query.model" v-on:change="changeFilter()">
                      <option value="">Select Model:</option>
                      <option v-if="query.make != ''"
                        v-for="model in parameters.model" 
                        :value="model.model" v-text="model.model">
                        </option>
                    </select>
                </div>
                <div class='zum-btn'>
                    <select v-model="query.trim" v-on:change="changeFilter()">
                      <option value="">Select Trim:</option>
                      <option v-if="query.model != ''"
                        v-for="trim in parameters.trim" 
                        :value="trim.ttrim" v-text="trim.ttrim">
                        </option>
                    </select>
                </div>
                <div class='zum-btn'>
                    <select v-model="query.engine">
                      <option value="">Select Engine:</option>
                      <option v-if="query.trim != ''"
                        v-for="engine in parameters.engine" 
                        :value="engine.engine" v-text="engine.engine">
                        </option>
                    </select>
                </div>
                <div class='zum-btn'>
                  <a class="checkout-button button alt wc-forward" 
                    v-on:click="checkCompatibility()"
                    style="background: #d1111c;">CHECK</a>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 0">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:#5ba71b;">This part is compatible with <span id="w1-33cttl" v-text="paginate.count"></span> vehicle(s).</span>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 2">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:red;">This part is not compatible with <span v-text="query.year"></span> <span v-text="query.make"></span> <span v-text="query.model"></span> <span v-text="query.trim"></span> <span v-text="query.engine"></span>
                  </span>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 1">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:#5ba71b;">This part is compatible with <span v-text="query.year"></span> <span v-text="query.make"></span> <span v-text="query.model"></span> <span v-text="query.trim"></span> <span v-text="query.engine"></span>
                  </span>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 2 || checkingCompatibility == 1">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <a v-on:click="resetSearch()" style="color: blue;">[show all compatible vehicles]</a>
                </div>
            </div>
        </div>
        <div class="zum-element">
         <table class=" zum-table shop_table woocommerce-checkout-review-order-table"
          v-if="checkingCompatibility == 0 && list.length > 0">
            <thead>
               <tr>
                  <th class="product-name">Year</th>
                  <th class="product-name">Make</th>
                  <th class="product-name">Model</th>
                  <th class="product-name">Trim</th>
                  <th class="product-name">Engine</th>
               </tr>
            </thead>
            <tbody>
               <tr class="cart_item" v-for="reg in list">
                  <td  v-text="reg.year"></td>
                  <td  v-text="reg.make"></td>
                  <td  v-text="reg.model"></td>
                  <td  v-text="reg.ttrim"></td>
                  <td  v-text="reg.engine"></td>
               </tr>
            </tbody>
         </table>
       </div>
   </div>

    <div class="zum-mobile" id="zum-prod-tab-1" v-if="true">
        <!-- https://www.w3schools.com/howto/tryit.asp?filename=tryhow_custom_select -->
        <div class='zum-element zum-container '>
            <div class='zum-row'>
                <div class='zum-btn'>
                  <select v-model="query.year">
                    <option value="">Select Year:</option>
                    <option 
                      v-for="year in parameters.year" 
                      :value="year.year" v-text="year.year">
                      </option>
                  </select>
                </div>
            </div>
            <div class='zum-row'>
                <div class='zum-btn'>
                   <select v-model="query.make" v-on:change="changeFilter()">
                    <option value="">Select Make:</option>
                    <option v-if="query.year != ''"
                      v-for="make in parameters.make" 
                      :value="make.make" v-text="make.make">
                      </option>
                  </select>
                </div>
            </div>
            <div class='zum-row'>
                <div class='zum-btn'>
                    <select v-model="query.model" v-on:change="changeFilter()">
                      <option value="">Select Model:</option>
                      <option v-if="query.make != ''"
                        v-for="model in parameters.model" 
                        :value="model.model" v-text="model.model">
                        </option>
                    </select>
                </div>
            </div>
            <div class='zum-row'>
                <div class='zum-btn'>
                    <select v-model="query.trim" v-on:change="changeFilter()">
                      <option value="">Select Trim:</option>
                      <option v-if="query.model != ''"
                        v-for="trim in parameters.trim" 
                        :value="trim.ttrim" v-text="trim.ttrim">
                        </option>
                    </select>
                </div>
            </div>
            <div class='zum-row'>
                <div class='zum-btn'>
                    <select v-model="query.engine">
                      <option value="">Select Engine:</option>
                      <option v-if="query.trim != ''"
                        v-for="engine in parameters.engine" 
                        :value="engine.engine" v-text="engine.engine">
                        </option>
                    </select>
                </div>
            </div>
            <div class='zum-row'>
                <div class='zum-btn'>
                  <br>
                  <a class="checkout-button button alt wc-forward" 
                    v-on:click="checkCompatibility()"
                    style="background: #d1111c;">CHECK</a>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 0">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:#5ba71b;">This part is compatible with <span id="w1-33cttl" v-text="paginate.count"></span> vehicle(s).</span>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 2">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:red;">This part is not compatible with <span v-text="query.year"></span> <span v-text="query.make"></span> <span v-text="query.model"></span> <span v-text="query.trim"></span> <span v-text="query.engine"></span>
                  </span>
                </div>
            </div>
        </div>

        <div class='zum-element zum-container' v-if="checkingCompatibility == 1">
            <div class='zum-row'>
                <div class='zum-btn'>
                  <span style="color:#5ba71b;">This part is compatible with <span v-text="query.year"></span> <span v-text="query.make"></span> <span v-text="query.model"></span> <span v-text="query.trim"></span> <span v-text="query.engine"></span>
                  </span>
                </div>
            </div>
        </div>

    </div>
  </div>
  <ul class="zum-element page-numbers zum-desktop" v-if="checkingCompatibility == 0">
    <li v-if="pagination.current_page > 1">
      <a class="prev page-numbers" 
        v-on:click='getList(pagination.current_page-1)'>
        <i class="fa fa-angle-left"></i>
      </a>
    </li>
    <!--<li><span aria-current="page" class="page-numbers current">1</span></li>-->

    <li v-for="page in pagination.left">
      <a class="page-numbers" 
      v-text="page" 
      v-on:click='getList(page)'></a>
    </li>
    
    <li v-if="pagination.left.length > 0">
      <span aria-current="page" 
      class="page-numbers"> ... </span>
    </li>

    <li v-for="page in pagination.center">
      <a class="page-numbers" 
      v-text="page" 
      v-if="pagination.current_page != page"
      v-on:click='getList(page)'></a>
      
      <span aria-current="page" 
      class="page-numbers current" 
      v-if="pagination.current_page == page"
      v-text="page"></span>

    </li>

    <li v-if="pagination.right.length > 0">
      <span aria-current="page" 
      class="page-numbers"> ... </span>
    </li>

   <li v-for="page in pagination.right">
      <a class="page-numbers" 
      v-text="page" 
      v-on:click='getList(page)'></a>
    </li>


    <li v-if="pagination.current_page < this.pagination.last_page">
      <a class="next page-numbers"
        v-on:click='getList(pagination.current_page+1)' >
        <i class="fa fa-angle-right"></i>
      </a>
    </li>
  </ul>
</div>
</div>
<script type="text/javascript">
    new Vue({
    el: '.zum-sync-sc-main',    
    created: function(){
        //this.getAll(1);   
        //this.element = this.elementInitialState();
        this.showLoading();
        this.post_id = jQuery('#zum-post-id').val();
        this.getParameters();
        this.getProductAttributes();
        this.getList(1);
        
    },
    updated: function(){
        //this.closeLoading();
    },
    mounted: function(){
        //this.alerta('mounted');
    },
    data:  {    
            post_id: '',
            parameters: {
                
            },
            attributes: {},
            query: {
              year:"",
              make:"",
              model:"",
              trim:"",
              engine:"",
              action:"", 
              post_id: ""
            },
            checkingCompatibility: 0,
            services:{
                getParameters: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=getParameters',
                getProductAttributes: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=getProductAttributes',
                getList: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=getList',
                checkCompatibility: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=checkCompatibility',
                autoSync: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=autoSync',
            },
            list: {},
            paginate: {},
            pagination: {
                'total'         :0,
                'current_page'  :0,
                'per_page'      :0,
                'last_page'     :0,
                'from'          :0,
                'to'            :0,
                'pages'         :[],
                'left'          :[],
                'center'        :[],
                'right'         :[],
                'max_round_pages':3, 
            },
            offset: 20,
            loading: 0,
        
    },
    methods: {      
        showLoading: function(){
          jQuery('.zum-element').each(function(){
            jQuery(this).hide();
          });
          jQuery('.zum-loading').show();
        },
        hideLoading: function(){
          jQuery('.zum-element').each(function(){
            jQuery(this).show();
          });
          jQuery('.zum-loading').hide();
        },
        alerta: function(msg){
            //alert(msg);
        },
        _jsonToForm: function(json){
            var form_data = new FormData();
            for ( var key in json ) {
                if(typeof(json[key]) == 'object'){
                    form_data.append(key, JSON.stringify(json[key]));
                }else{
                    form_data.append(key, json[key]);
                }
            }
            return form_data;
        },
        getParameters: function(){
            
            var service = this.services.getParameters;
            service += '&post_id='+this.post_id;
            service += '&make='+this.query.make;
            service += '&model='+this.query.model;
            service += '&trim='+this.query.trim;
            service += '&engine='+this.query.engine;
            this.query.action = 'getParameters';
            this.query.post_id = this.post_id;
            var json = this.query;
            // this._jsonToForm(json)
            axios.get(service, json
                ).then(response => {
                if(response.data.status == 'OK'){
                    this.parameters = response.data.parameters;
                }else{

                }
            }).catch(error => {
                console.log(error);
            }); 
        },
        getList: function(page){
          this.showLoading();
            this.pagination.current_page = page;
            var service = this.services.getList;
            service += '&post_id='+this.post_id+'&page='+page;
            var json = {action:'getList', post_id: this.post_id}
            // this._jsonToForm(json)
            axios.get(service, json
                ).then(response => {
                if(response.data.status == 'OK'){
                  if(response.data.list.length == 0){
                    this.syncElement();
                  }else{
                    this.list = response.data.list;
                    this.paginate = response.data.paginate;
                    this.setPaginate();
                    jQuery('.zum-table').css('display','inline');
                    this.hideLoading();
                  }
                }
            }).catch(error => {
                
            }); 
        },
        getProductAttributes: function(){
            var service = this.services.getProductAttributes;
            service += '&post_id='+this.post_id;
            var json = {action:'getProductAttributes', post_id: this.post_id}
            // this._jsonToForm(json)
            axios.get(service, json
                ).then(response => {
                if(response.data.status == 'OK'){
                  this.attributes = response.data.data;
                  jQuery('#zum-sync-sc-info').show();
                }else{
                  jQuery('#zum-sync-sc-info').hide();
                }
            }).catch(error => {
                
            }); 
        },
        syncElement: function(){
            //console.log(page);
            
            var service = this.services.autoSync;
            service += '&post_id='+this.post_id;
            var json = {action:'autoSync', post_id: this.post_id}
            // this._jsonToForm(json)
            axios.get(service, json
                ).then(response => {
                
                if(response.data.status == 'OK'){
                  if(response.data.list.length > 0){
                    this.list = response.data.list;
                    this.paginate = response.data.paginate;
                    this.setPaginate();
                    jQuery('.zum-table').css('display','inline');
                    this.getParameters();
                  }
                }
                this.hideLoading();
            }).catch(error => {
                
            }); 
        },
        setPaginate: function(){
          var pages = [];
          this.pagination.last_page = Math.ceil(this.paginate.count/this.paginate.per_page);
          for (var i = 1; i <= this.pagination.last_page; i++) {
            pages.push(i);
          }
          this.pagination.center = pages;
          /*
            3 izq
            current
            3 der
            =
            3izq*2 + current
          */
          if(this.pagination.max_round_pages*2+1 > this.pagination.last_page){
            return;
          }
          //left numbers
          this.pagination.left = [];
          if(this.pagination.current_page > this.pagination.max_round_pages){
            var j = 0;
            pages = [];
            for (var i = 1; i <= this.pagination.max_round_pages; i++) {
              if(i < this.pagination.current_page - this.pagination.max_round_pages){
                pages.push(i);
              }
            }
            this.pagination.left = pages;
          }
          //center numbers
          pages = [];
          for (var i = this.pagination.current_page - this.pagination.max_round_pages; i <= this.pagination.current_page + this.pagination.max_round_pages ; i++) {
            //console.log(i+'-');
            if(i > 0){
              if(i <= this.pagination.last_page){
                pages.push(i);
              }
            }
          }
          this.pagination.center = pages;

          //right numbers
          this.pagination.right = [];
          if(this.pagination.current_page + this.pagination.max_round_pages < this.pagination.last_page)
          {
            var j = 0;
            for (var i = this.pagination.last_page - this.pagination.max_round_pages+1; i <= this.pagination.last_page ; i++) {
              if(i >= this.pagination.current_page + this.pagination.max_round_pages + 1){
                this.pagination.right.push(i);
              }
            }
          }
        },
        // setPaginate: function(){
        //   var pages = [];
        //   this.pagination.last_page = Math.ceil(this.paginate.count/this.paginate.per_page);
        //   for (var i = 1; i <= this.pagination.last_page; i++) {
        //     pages.push(i);
        //   }
        //   this.pagination.pages = pages;
        // },
        changeFilter: function(){
          this.getParameters();
        },
        checkCompatibility: function(){
          
          var service = this.services.checkCompatibility;
          service += '&post_id='+this.post_id;
          service += '&year='+this.query.year;
          service += '&make='+this.query.make;
          service += '&model='+this.query.model;
          service += '&trim='+this.query.trim;
          service += '&engine='+this.query.engine;
          this.query.action = 'checkCompatibility';
          this.query.post_id = this.post_id;
          var json = this.query;
          // this._jsonToForm(json)
          axios.get(service, json
              ).then(response => {
                
              if(response.data.status == 'OK'){
                  this.checkingCompatibility = 1;
              }else{
                  this.checkingCompatibility = 2;
                  
              }
          }).catch(error => {
              console.log(error);
          }); 
        },
        resetSearch: function(){

            this.query = {
              year:"",
              make:"",
              model:"",
              trim:"",
              engine:"",
              action:"", 
              post_id: ""
            };
            this.checkingCompatibility = 0;
            this.getParameters();
            this.getList(1);
        }
    }


});
</script>

    <?php
}
add_action( 'zum_sync_sc_fitments', 'zum_sync_sc_fitments', 200 );

/*
INSTALATION PROCESS
hay un archivo dentro de woocommerce llamado content-single-product

zum_sync_sc_fitments($post->ID); 
*/

?>