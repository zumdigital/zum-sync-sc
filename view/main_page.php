<?php

//require_once('wp-config.php');
//require_once('wp-config.php');
require_once __DIR__ .'/../model/product.php';

/*
$_product = new Product();
$_product->update_missing_products();
$products = $_product->get_new_products();
echo count($products['data']);
echo json_encode($products);*/

//http://webservices.solidcommerce.com/ws.asmx/GetAllEbayFitments?appKey=bLkegLsmde%23C2%3A%21p&securityKey=D69_954647222Z5*c59tJH85Ud79%2Bt9GE5J5%21m63t376T8%3ATgm

// function httpGet($url){
//     $ch = curl_init();  
//     curl_setopt($ch,CURLOPT_URL,$url);
//     curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
//     $output=curl_exec($ch);
//     curl_close($ch);
//     return $output;
// }
// //webservices.solidcommerce.com/ws.asmx/GetProduct?appKey=bLkegLsmde%23C2%3A%21p&xslUri= &productID=620&includeExtendedImages=false
// //$url = 'http://webservices.solidcommerce.com/ws.asmx/GetAllEbayFitments?appKey=bLkegLsmde%23C2%3A%21p&securityKey=D69_954647222Z5*c59tJH85Ud79%2Bt9GE5J5%21m63t376T8%3ATgm';
// $url = 'http://webservices.solidcommerce.com//ws.asmx/GetProduct?appKey=bLkegLsmde%23C2%3A%21p&xslUri=&productID=1152&includeExtendedImages=false';
// $xml = simplexml_load_file($url, 'SimpleXMLElement',LIBXML_NOCDATA);
// print_r($xml);

?>

<style>
    #overlay {
        background: #ffffff;
        color: #666666;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 100000;
        top: 0;
        left: 0;
        float: left;
        text-align: center;
        padding-top: 25%;
    }
    .overlay_img{
    	height: 50%;
    }
</style>
<div id="main">
	<div id="overlay">
	    <img class="overlay_img" src="<?php echo WP_PLUGIN_URL ?>/zum-sync-sc/img/Loading.gif" alt="Loading" /><br/>
	</div>

	<input type="hidden" id="zum-sync-sc_plugin_path" value="<?php echo WP_PLUGIN_URL ?>">

	<div class="container">
		
		
		<div class="row">
			<div class="col">
				<h3 id="table-title">Vehicle List (All[<span v-text='paginate.count'></span>])</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				
			<table class="table table-sm table-striped">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Name</th>
			      <th scope="col">SKU</th>
			      <th scope="col">Post</th>
			      <th scope="col">Fecha</th>
			      <th scope="col">Status</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	
			    <tr class="registro" v-for="reg in list">
			      <td v-text="reg.id"></td>
			      <td v-html="reg.product_name.substring(0,35)"></td>
			      <td v-text="reg.product_sku"></td>
			      <td v-text="reg.post_id"></td>
			      <td v-text="reg.last_updated_date"></td>
			      <td>
			      	
			      		<span
			      			class=" btn btn-warning btn-sm text-white"
			      			v-if="reg.status == 'Pending'"
			      			v-text='reg.status'
			      			>
			      			Pending
			      		</span>
			      		<span
			      			class=" btn btn-success btn-sm text-white"
			      			v-if="reg.status != 'Pending'"
			      			v-text='reg.status'
			      			>
			      			Ready
			      		</span>
			      </td>
			      <td>
			      	<a class=" btn btn-primary btn-sm text-white" 
			      		@click.prevent="syncElement(reg)">
			      			Sync Again
			      	</a>
			      </td>
			    </tr>
			   
			  </tbody>
			</table>
			
			
			</div>
		</div>
	</div>

	<div class="modal" tabindex="-1" role="dialog" id='viewModal'>
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Element view</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="container-fluid">
			    <div class="row">
			      <div class="col-md-3">Name</div>
			      <div class="col-md-9" v-text="element.product_name"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">SKU</div>
			      <div class="col-md-9" v-text="element.product_sku"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">YEAR</div>
			      <div class="col-md-9" v-text="element.year"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">MAKE</div>
			      <div class="col-md-9" v-text="element.make"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">MODEL</div>
			      <div class="col-md-9" v-text="element.model"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">TTRIM</div>
			      <div class="col-md-9" v-text="element.ttrim"></div>
			    </div>
			    <div class="row">
			      <div class="col-md-3">ENGINE</div>
			      <div class="col-md-9" v-text="element.engine"></div>
			    </div>

			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

<nav aria-label="Page navigation example">
  <ul class="pagination">
    
    <li class="page-item"
    	v-if="pagination.current_page > 1">
      <a class="page-link" 
      		v-on:click='getList(pagination.current_page-1)'>
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
 
      </a>
    </li>

    <li class="page-item"
    	 v-for="page in pagination.left">
      <a class="page-link" 
      v-text="page" 
      v-on:click='getList(page)'></a>
    </li>
    
    <li class="page-item"
    	 v-if="pagination.left.length > 0">
      <span aria-current="page" 
      class="page-numbers"> ... </span>
    </li>

    <li class="page-item" v-for="page in pagination.center">
	      <a class="page-link" 
	      v-if="pagination.current_page != page"
	      v-text="page" 
	      v-on:click='getList(page)'></a>

	      <a class="page-link" style="color:red;" 
	      v-if="pagination.current_page == page"
	      v-text="page" 
	      v-on:click='getList(page)'></a>
	      
	    </li>

    <li class="page-item" v-if="pagination.right.length > 0">
      <span aria-current="page" 
      class="page-link"> ... </span>
    </li>

   <li class="page-item" v-for="page in pagination.right">
      <a class="page-link" 
      v-text="page" 
      v-on:click='getList(page)'></a>
    </li>

    <li class="page-item" v-if="pagination.current_page < this.pagination.last_page">
      <a class="page-link" aria-label="Next"
      	v-on:click='getList(pagination.current_page+1)'
      	>
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
  </ul>
</nav>

</div>	







<script type="text/javascript">
	
Vue.mixin({
	data: function () {
	    return {
	    	
	    }
	},
	computed:{
		isActived: function(){
			return this.pagination.current_page;
		},
		pagesNumber: function(){
			if(!this.pagination.to){
				return [];
			}

			var from = this.pagination.current_page - this.offset;
			if(from < 1){
				from = 1;
			}

			var to = from + (this.offset * 2)
			if(to > this.pagination.last_page){
				to = this.pagination.last_page;
			}

			var pagesArray = [];
			while(from <= to){
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
		},
		lastShowElement: function(){
			var from =  this.pagination.from;
			var to = 0;
			if(from + 19 > this.pagination.total){
				to = this.pagination.total;
			}else{
				to = from + 19;
			}
			return to;
		}
	},
	methods:{
		init: function(){

		},
		closeLoading: function(){
			$('#overlay').hide();
		},
		openLoading: function(){
			$('#overlay').show();
		},
	},
});







new Vue({
	el: '#main',	
	created: function(){
		this.getList(1); 	
		this.element = this.elementInitialState();
	},
	updated: function(){
		this.closeLoading();
	},
	mounted: function(){
	},
	data:  {	
			notification: {},
			register: false,	
			parameters: {
				brands: [],
				lines: [],
				cities: []
			},
			services:{
	    		getAll: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=getAll',
	    		syncElement: 'https://carsuperservices.com/wp-content/plugins/zum-sync-sc/controller/api.php?action=sync',
	    	},
	      	list: {},
			element: {},
			paginate: {},
            pagination: {
                'total'         :0,
                'current_page'  :0,
                'per_page'      :0,
                'last_page'     :0,
                'from'          :0,
                'to'            :0,
                'pages'         :[],
                'left'          :[],
                'center'        :[],
                'right'         :[],
                'max_round_pages':3, 
            },
            offset: 20,
            loading: false,
		
	},
	methods: {		
		elementInitialState: function(){
			return {};
		},
		_jsonToForm: function(json){
            var form_data = new FormData();
            for ( var key in json ) {
                if(typeof(json[key]) == 'object'){
                    form_data.append(key, JSON.stringify(json[key]));
                }else{
                    form_data.append(key, json[key]);
                }
            }
            return form_data;
        },
		getList: function(page){
			this.pagination.current_page = page;
			var service = this.services.getAll;
            service += '&page='+page;
            var json = {action:'getAll'}
			axios.get(service, {action:'getAll'}
				).then(response => {
				if(response.data.status == 'OK'){
					this.list = response.data.list;
					this.paginate = response.data.paginate;
                    this.setPaginate();
				}else{

				}
			}).catch(error => {
			    console.log(error);
			});	
		},
		syncElement: function(reg){
			var service = this.services.syncElement;
			axios.post(service, this._jsonToForm(reg)
				).then(response => {
				if(response.data.status == 'OK'){
					this.getList(1);
					//alert(response.data.message);
				}else{

				}
			}).catch(error => {
			    console.log(error);
			});	
		},
		setPaginate: function(){
          var pages = [];
          this.pagination.last_page = Math.ceil(this.paginate.count/this.paginate.per_page);
          for (var i = 1; i <= this.pagination.last_page; i++) {
            pages.push(i);
          }
          this.pagination.center = pages;
          /*
            3 izq
            current
            3 der
            =
            3izq*2 + current
          */
          if(this.pagination.max_round_pages*2+1 > this.pagination.last_page){
            return;
          }
          //left numbers
          this.pagination.left = [];
          if(this.pagination.current_page > this.pagination.max_round_pages){
            var j = 0;
            pages = [];
            for (var i = 1; i <= this.pagination.max_round_pages; i++) {
              if(i < this.pagination.current_page - this.pagination.max_round_pages){
                pages.push(i);
              }
            }
            this.pagination.left = pages;
          }
          //center numbers
          pages = [];
          for (var i = this.pagination.current_page - this.pagination.max_round_pages; i <= this.pagination.current_page + this.pagination.max_round_pages ; i++) {
            //console.log(i+'-');
            if(i > 0){
              if(i <= this.pagination.last_page){
                pages.push(i);
              }
            }
          }
          this.pagination.center = pages;

          //right numbers
          this.pagination.right = [];
          if(this.pagination.current_page + this.pagination.max_round_pages < this.pagination.last_page)
          {
            var j = 0;
            for (var i = this.pagination.last_page - this.pagination.max_round_pages+1; i <= this.pagination.last_page ; i++) {
              if(i >= this.pagination.current_page + this.pagination.max_round_pages + 1){
                this.pagination.right.push(i);
              }
            }
          }
        },

	}


});

</script>