<?php 

	class Product{
		//$db_table_name = 'ext_rtb_place';

		function get_woocommerce_products(){
			global $wpdb;
			$sql = "SELECT 
					   posts.post_title, postmeta.meta_value as sku, posts.ID
					FROM
					    ".$wpdb->prefix."posts as posts, ".$wpdb->prefix."postmeta as postmeta
					    WHERE postmeta.meta_key = '_sku'
					    	AND postmeta.post_id = posts.ID
					    	AND posts.post_type ='product' ";
			/*
			
			*/
			
			$results = $wpdb->get_results( $sql, OBJECT );
			//return $wpdb->last_error;
			//return $results;
			if(count($results) == 0){
				$result = array("status" => 'ER', 'message' => 'no se pudo recueperar la programación' );
			}else{
				$result = array("status" => 'OK', 'data' => $results );
			}
			return $result;
		}

		function getProductZum($post_id){
			global $wpdb;
			$sql = "SELECT 
					   *
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments
					    WHERE zum_sync_sc_fitments.post_id = '".$post_id."'";
			/*
			
			*/
			// echo $sql;
			$results = $wpdb->get_results( $sql, OBJECT );
			//return $wpdb->last_error;
			//return $results;
			if(count($results) == 0){
				$result = array("status" => 'ER', 'message' => 'no se pudo recueperar el programación' );
			}else{
				$result = array("status" => 'OK', 'data' => $results );
			}
			return $result;
		}

		function getProductAttributesWithNames($product){
			$data = [];
			$data['condition_description'] = 'Condition Description';
			$data['image_notes'] = 'Image Notes';
			$data['condition_general'] = 'Condition';
			$data['model'] = 'Model';
			$data['features'] = 'Features';
			$data['primary_color'] = 'Primary Color';
			$data['surface_finish'] = 'Surface Finish';
			$data['color_code'] = 'Color Code';
			$data['brand'] = 'Brand';
			$data['model_year'] = 'Model Year';
			$data['manufacturer_part_number'] = 'Manufacturer Part Number';
			$data['other_part_number'] = 'Other Part Number';
			$data['interchange_part_number'] = 'Interchange Part Number';
			$data['placement_on_vehicle'] = 'Placement On Vehicle';
			$data['donator'] = 'Donator';
			$data['vin_number'] = 'Vin Number';
			$data['return_general'] = 'Return';
			$data['oem'] = 'OEM';


			$result = [];
			$product = $product['data'][0];
			$json_string = json_encode($product);    
			$product = json_decode($json_string, TRUE);
			//var_dump($product);
			foreach ($data as $key => $value) {
				//echo $key.'='.$value.'='.$product[$key];
				if(isset($product[$key]) && $product[$key] != ''){
					$element = [];
					$element['name'] = $value; 
					$element['value'] = $product[$key]; 
					array_push($result, $element);
				}
			}

			return array('status' => 'OK', 'data'=>$result);
		}


		function update_missing_products(){
			$products = $this->get_woocommerce_products();
			$current_products = $this->getCurrentProductsIds();
			//echo json_encode($products).'<br>';
			//echo json_encode($current_products);
			if($products['status'] == 'OK' && count($products['data']) > 0){
				foreach ($products['data'] as $product) {
					if(!in_array($product->sku, $current_products)){
						$this->set_product_from_woocommerce($product->ID, $product->post_title, $product->sku);
					}
				}
			}
		}


		function set_timestamps(&$data){
			$data['date_begin_numbers'] =  strtotime( $data['date_begin']." +".$data['hour_begin']." hours" );
			$data['date_ends_numbers'] =  strtotime( $data['date_ends']." +".$data['hour_ends']." hours" );
		}


		function getSpecifics($product){
			$data = [];
			$data['condition_description'] = '';
			$data['image_notes'] = '';
			$data['condition_general'] = '';
			$data['model'] = '';
			$data['features'] = '';
			$data['primary_color'] = '';
			$data['surface_finish'] = '';
			$data['color_code'] = '';
			$data['brand'] = '';
			$data['model_year'] = '';
			$data['manufacturer_part_number'] = '';
			$data['other_part_number'] = '';
			$data['interchange_part_number'] = '';
			$data['placement_on_vehicle'] = '';
			$data['donator'] = '';
			$data['vin_number'] = '';
			$data['return_general'] = '';
			$data['oem'] = '';

			if($product['status'] != 'OK'){
				return $data;
			}
			
			//armamos un array de specifics
			$specifics = [];
			$product = $product['product'];
			$json_string = json_encode($product);    
			$result_array = json_decode($json_string, TRUE);
			
			foreach($result_array['Specifics']['Specific'] as $specific){
				//echo json_encode($specific);
			    $specifics[strtolower($specific['Name'])] = $specific['Value'];
			}

			// foreach ($specifics as $key => $value) {
			// 	echo 'key:'.$key.'='.$value;
			// }
			foreach ($data as $key => $value) {
				if(isset($specifics[$key])){
					$data[$key] = $specifics[$key];
				}
			}
			// echo json_encode($data);
			// echo json_encode($specifics);
			return $data;
		}

		function set_product_from_woocommerce($id, $name, $sku){
			
			//echo 'actualizando';
			$fitmentObj = new Fitment();
			$product = $fitmentObj->getProduct($sku);
			
			$data = $this->getSpecifics($product);
			//exit(2);
			/*$errors = $this->validar($data);
			if(count($errors) > 0){
				return array('status' => 'ER', 'message'=>implode('\n',$errors));
			}*/
			global $wpdb;

			$data['product_name'] = $name;
			$data['product_sku'] = $sku;
			$data['last_updated_date'] = date("Y-m-d H:i:s");
			$data['post_id'] = $id;

			// $data_array = array('product_name'=> $name,
			// 				'product_sku'=> $sku,
			// 				'last_updated_date'=> date("Y-m-d H:i:s"),
			// 				'post_id' => $id);
			
			// $result = $wpdb->insert($wpdb->prefix."zum_sync_sc_fitments", $data_array);
			$result = $wpdb->insert($wpdb->prefix."zum_sync_sc_fitments", $data);
			if(!$result){
				return array('status' => 'ER', 'message'=>'error registering product for fitment'
							, 'trace' => $wpdb->last_error);
			}

			$data_array = array('meta_key'=> '_zum_processed',
							'meta_value'=> 's',
							'post_id' => $id);
			
			$result = $wpdb->insert($wpdb->prefix."postmeta", $data_array);
			if(!$result){
				return array('status' => 'ER', 'message'=>'error registering product for fitment'
							, 'trace' => $wpdb->last_error);
			}


			return array('status' => 'OK', 'message'=>'success registering product for fitment');
		}

		function update_product_from_woocommerce($id, $name, $sku){
			
			//echo 'actualizando';
			$fitmentObj = new Fitment();
			$product = $fitmentObj->getProduct($sku);
			
			$data = $this->getSpecifics($product);
			

			global $wpdb;

			$data['product_name'] = $name;
			$data['product_sku'] = $sku;
			$data['last_updated_date'] = date("Y-m-d H:i:s");
			
			$result = $wpdb->update($wpdb->prefix.'zum_sync_sc_fitments'
				, $data
				, array('id'=>$id));

			// $data_array = array('product_name'=> $name,
			// 				'product_sku'=> $sku,
			// 				'last_updated_date'=> date("Y-m-d H:i:s"),
			// 				'post_id' => $id);
			
			// $result = $wpdb->insert($wpdb->prefix."zum_sync_sc_fitments", $data_array);
			//$result = $wpdb->insert($wpdb->prefix."zum_sync_sc_fitments", $data);
			if(!$result){
				return array('status' => 'ER', 'message'=>'error updating product for fitment'
							, 'trace' => $wpdb->last_error);
			}
			

			return array('status' => 'OK', 'message'=>'success updating product for fitment');
		}

		function getCurrentProductsIds(){
			global $wpdb;
			$sql = "SELECT 
					   product_sku
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments
					    ORDER BY id desc";
			$results = $wpdb->get_results( $sql, OBJECT );
			$array = [];
			if(count($results) > 0){
				foreach ($results as $result) {
					$array[] = $result->product_sku;
 				}
			}
			//return $wpdb->last_error;
			//return $results;
			return $array;
		}

		function getCurrentSkuByPostId($post_id){
			global $wpdb;
			$sql = "SELECT 
					   id,
					   product_sku
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments
					WHERE
						post_id = ".$post_id."
					    ORDER BY id desc";
			$results = $wpdb->get_results( $sql, OBJECT );
			$array = [];
			if(count($results) > 0){
				foreach ($results as $result) {
					return $result;
 				}
			}
			//return $wpdb->last_error;
			//return $results;
			return false;
		}

		function getAll($current_page = 1, $offset = 0, $per_page = 20){
			$offset = $current_page * $per_page - $per_page;
			$this->update_missing_products();
			global $wpdb;
			$sql = "SELECT 
					   *
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments
					    ORDER BY id desc";
			/*
			
			*/
			
			$list = $wpdb->get_results( $sql, OBJECT );
			$count = count($list);
			//$paginate = $this->paginate($count, $current_page, $per_page);
			$paginate = array('page' => $current_page,'offset' => $offset, 'per_page' => $per_page, 'count' => $count);

			$sql .= ' LIMIT '.$offset.', '.$per_page;
			//echo $sql;
			$list = $wpdb->get_results( $sql, OBJECT );
			
			return array("status" => 'OK', 'list' => $list, 'paginate' => $paginate);
		}

	}

