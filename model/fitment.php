<?php 

require_once('../../../../wp-config.php');
require_once('../../../../wp-load.php');

class Fitment{

	public function soap_object($url){

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$contents = curl_exec($ch);
		if (curl_errno($ch)) {
		  echo curl_error($ch);
		  echo "\n<br />";
		  $contents = '';
		} else {
		  curl_close($ch);
		}

		if (!is_string($contents) || !strlen($contents)) {
		echo "Failed to get contents.";
		$contents = '';
		}
		/*header("Content-type: text/xml; charset=utf-8");
		echo $contents;*/

		$xml = simplexml_load_string($contents, null, LIBXML_NOCDATA);

		
		return $xml;
	}

	public function getParameters($post_id = '', $imake, $imodel, $itrim, $iengine){
		if($post_id == ''){
			return array('status'=>'ERROR', 'message' =>'we cant recognize the current post');
		}
		global $wpdb;
		// year
		$sql = "SELECT 
				   DISTINCT zum_sync_sc_compatibilities.year
				FROM
				    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
				    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
				    WHERE zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
				    ORDER BY zum_sync_sc_compatibilities.year desc";
		// AND zum_sync_sc_fitments.post_id = ".$post_id."				    
		$year = $wpdb->get_results( $sql, OBJECT );
		// make
		
		
		$sql = "SELECT 
				   DISTINCT zum_sync_sc_compatibilities.make
				FROM
				    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
				    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
				    WHERE zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
				    ORDER BY zum_sync_sc_compatibilities.make asc";
				  	//AND zum_sync_sc_fitments.post_id = ".$post_id."
		$make = $wpdb->get_results( $sql, OBJECT );
		
		// model
		if($imake != ''){
			$sql = "SELECT 
					   DISTINCT zum_sync_sc_compatibilities.model
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
					    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
					    WHERE zum_sync_sc_compatibilities.make = '".$imake."'
					    and zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
					    ORDER BY zum_sync_sc_compatibilities.model asc";
					    //AND zum_sync_sc_fitments.post_id = ".$post_id."
			$model = $wpdb->get_results( $sql, OBJECT );
			//$model =$wpdb->last_query;
		}else{
			$model = [];
		}

		// trim
		if($imodel != ''){
			$sql = "SELECT 
					   DISTINCT zum_sync_sc_compatibilities.ttrim
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
					    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
					    WHERE zum_sync_sc_compatibilities.model = '".$imodel."'
					    and zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
					    ORDER BY zum_sync_sc_compatibilities.ttrim asc";
					    //AND zum_sync_sc_fitments.post_id = ".$post_id."
			$trim = $wpdb->get_results( $sql, OBJECT );
			//$trim =$wpdb->last_query;
		}else{
			$trim = [];
		}
		// engine
		if($itrim != ''){
			$sql = "SELECT 
					   DISTINCT zum_sync_sc_compatibilities.engine
					FROM
					    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
					    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
					    WHERE zum_sync_sc_compatibilities.ttrim = '".$itrim."' 
					    and zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
					    ORDER BY zum_sync_sc_compatibilities.engine asc";
					    //AND zum_sync_sc_fitments.post_id = ".$post_id."
			$engine = $wpdb->get_results( $sql, OBJECT );
		}else{
			$engine = [];
		}

		return array("status" => 'OK', 'parameters' => [
			'year' => $year,
			'make' => $make,
			'model' => $model,
			'trim' => $trim,
			'engine' => $engine
		]);
	}

	public function checkCompatibility($post_id = '', $iyear, $imake, $imodel, $itrim, $iengine){
		if($post_id == ''){
			return array('status'=>'ERROR', 'message' =>'we cant recognize the current post');
		}
		global $wpdb;
		
		// model
		$sql = "SELECT 
				   DISTINCT zum_sync_sc_compatibilities.model
				FROM
				    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
				    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
				    WHERE zum_sync_sc_compatibilities.engine = '".$iengine."'
				    and zum_sync_sc_compatibilities.ttrim = '".$itrim."'
				    and zum_sync_sc_compatibilities.model = '".$imodel."'
				    and zum_sync_sc_compatibilities.make = '".$imake."'
				    and zum_sync_sc_compatibilities.year = '".$iyear."'
				    and zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
				    ORDER BY zum_sync_sc_compatibilities.model asc";
				    //AND zum_sync_sc_fitments.post_id = ".$post_id."
		$model = $wpdb->get_results( $sql, OBJECT );

		if(count($model) > 0){
			return array("status" => 'OK', 'message' => '');
		}else{
			return array("status" => 'ERROR', 'message' => '', 'query' => $wpdb->last_query);
		}
	}

	public function getList($post_id = '', $current_page = 1, $offset = 0, $per_page = 10){
		$offset = $current_page * $per_page - $per_page;
		if($post_id == ''){
			return array('status'=>'ERROR', 'message' =>'we cant recognize the current post');
		}
		global $wpdb;
		
		$sql = "SELECT 
				  zum_sync_sc_compatibilities.*
				FROM
				    ".$wpdb->prefix."zum_sync_sc_fitments as zum_sync_sc_fitments,
				    ".$wpdb->prefix."zum_sync_sc_compatibilities as zum_sync_sc_compatibilities
				    WHERE zum_sync_sc_compatibilities.id_fitment = zum_sync_sc_fitments.id
				    AND zum_sync_sc_fitments.post_id = ".$post_id."
				    ORDER BY zum_sync_sc_compatibilities.year desc";
		$list = $wpdb->get_results( $sql, OBJECT );
		$count = count($list);
		//$paginate = $this->paginate($count, $current_page, $per_page);
		$paginate = array('page' => $current_page,'offset' => $offset, 'per_page' => $per_page, 'count' => $count);

		$sql .= ' LIMIT '.$offset.', '.$per_page;
		$list = $wpdb->get_results( $sql, OBJECT );
		
		return array("status" => 'OK', 'list' => $list, 'paginate' => $paginate);
	}

	public function paginate($count, $current_page, $per_page){
		$pageObj = 1;
		$paginate = array();
		/*for ($i=0; $i < $count + $per_page; $i+$per_page) { 
			$pageObj = array('page' => $page,'offset' => $i, 'per_page' => $per_page);	
			$page++;
			array_push($paginate, $pageObj);
		}*/
		return $paginate;
	}
	
	public function syncElement($id,$sku){
		$product = $this->getProduct($sku);
		if($product['status'] == 'ERROR'){
			return $product;
		}
		
		$product = $product['product'];
		
		$productObj = new Product();

		$productObj->update_product_from_woocommerce($id, $product->ProductName, $sku);

		
		// echo 'diego4:'.$product->OfferID;
		// echo 'diego4:'.$product->ManufactureModelNumber;
		//echo 'producto obtenido';
		//$ManufactureModelNumber = $product->ManufactureModelNumber;
		//$manufactureModelNumber = reset($product->ManufactureModelNumber);
		//echo json_encode($manufactureModelNumber);
		$fitments = $this->getFitmentsByProduct($product->ManufactureModelNumber);
		//return $fitments;
		$result = array();
		 $fitments = $fitments['fitments'];
		// echo json_encode($fitments);
		$this->deleteByFitment($id);
		
		if(count($fitments) > 0){
			foreach ($fitments->Fitment as $fitment) {
				//return $value;
				//echo 'cantidad:'.$fitment->Year;
				$this->set_fitment_from_solidcommerce($id, $fitment->Year, $fitment->Make, $fitment->Model, $fitment->Trim, $fitment->Engine);
				//return $value['Engine'];
			}
			$this->change_status($id);
		}
		return array('status'=>'OK', 'message' =>'All Fitments was synchronized');
	}

	private function getFitmentsByProduct($manufactureModelNumber){
		//echo 'getFitmentsByProduct<br>\n';
		$appKey = 'bLkegLsmde%23C2%3A%21p';
		$securityKey = 'D69_954647222Z5*c59tJH85Ud79%2Bt9GE5J5%21m63t376T8%3ATgm';

		// http://webservices.solidcommerce.com/ws.asmx/LoadEbayFitmentData
		// http://webservices.solidcommerce.com/ws.asmx/LoadEbayFitmentData?appKey=bLkegLsmde%23C2%3A%21p&securityKey=D69_954647222Z5*c59tJH85Ud79%2Bt9GE5J5%21m63t376T8%3ATgm&fitmentGroupName=604&categoryID=&isFitmentGroupID=true

		$url = 'http://webservices.solidcommerce.com/ws.asmx/LoadEbayFitmentData?appKey='.$appKey.'&securityKey='.$securityKey.'&fitmentGroupName='.$manufactureModelNumber.'&categoryID=&isFitmentGroupID=true';
		//echo $url.'<br>';
		$xml = $this->soap_object($url);
		// echo $xml->EbayCategory;
		// foreach ($xml->Fitment as $fitment) {
		// 	echo $fitment->Make.':';
		// }
		// $json = json_encode($xml);

		// $array = json_decode($json,TRUE);
		// //return $array;
		
		// if(isset($array->Error)){
		// 	return array('status'=>'ERROR', 'message' =>"we can't get the fitments", 'trace' => $url);
		// }
		// if(isset($array['Fitment'])){
		// 	return array('status'=>'OK', 'message' =>'', 'fitments' =>$array['Fitment'], 'trace' => $url);
		// }
		if (empty((array) $xml)) {
    		return array('status'=>'ERROR', 'message' =>"we can't get the fitments", 'trace' => $url);
		}
		return array('status'=>'OK', 'message' =>'', 'fitments' => $xml, 'trace' => $url);
		return array();
	}

	public function getProduct($sku){
		$appKey = 'bLkegLsmde%23C2%3A%21p';
		$url = 'http://webservices.solidcommerce.com//ws.asmx/GetProduct?appKey='.$appKey.'&xslUri=&productID='.$sku.'&includeExtendedImages=false';
		//echo $url;
		
		// $xml = simplexml_load_file($url, 'SimpleXMLElement',LIBXML_NOCDATA);
		// echo 'ver'. json_encode($xml);
		$xml = $this->soap_object($url);

		$product = $xml->Product;
		if (empty((array) $product)) {
    		return array('status'=>'ERROR', 'message' =>"we can't get the product");
		}
		// if(isset($xml->Error)){
		// 	return array('status'=>'ERROR', 'message' =>"we can't get the product");
		// }
		return array('status'=>'OK', 'message' =>'', 'product' =>$product);
	}

	private function set_fitment_from_solidcommerce($id_fitment, $year, $make, $model, $ttrim, $engine){
			global $wpdb;

			$data_array = array('id_fitment'=> $id_fitment,
							'year'=> $year,
							'make'=> $make,
							'model'=> $model,
							'ttrim'=> $ttrim,
							'engine'=> $engine);
			//echo json_encode($data_array);
			
			$result = $wpdb->insert($wpdb->prefix."zum_sync_sc_compatibilities", $data_array);
			//echo json_encode($result);
			if(!$result){
				return array('status' => 'ER', 'message'=>'error getting products fitment'
							, 'trace' => $wpdb->last_error);
			}
		}

	private function deleteByFitment($id_fitment){
		
		global $wpdb;

		$result = $wpdb->delete($wpdb->prefix.'zum_sync_sc_compatibilities'
			, array('id_fitment'=>$id_fitment));
		
		if(!$result){
			return array('status' => 'ER', 'message'=>'we cant delete fitments');
		}
		return array('status' => 'OK', 'message'=>'All fitments was deleted');
	}

	private function change_status($id){
			
			global $wpdb;
			$data_array = array('status'=> 'Synchronized');
			
			$result = $wpdb->update($wpdb->prefix.'zum_sync_sc_fitments'
				, $data_array
				, array('id'=>$id));
			
			return array('status' => 'OK', 'message'=>'this product was updated!');
		}

	private function httpGet($url){
	    $ch = curl_init();  
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    $output=curl_exec($ch);
	    curl_close($ch);
	    return $output;
	}

}

?>