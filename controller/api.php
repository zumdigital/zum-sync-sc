<?php 
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');

//require_once '../model/bannerschedule.php';
require_once __DIR__ .'/../model/product.php';
require_once __DIR__ .'/../model/fitment.php';

global $wpdb;

$action = '';

if(isset($_GET['action'])){
	$action = $_GET['action'];
}

if(isset($_POST['action'])){
	$action = $_POST['action'];
}



if($action == ''){
	$resultado = array('status'=>'ERROR', 'message' =>'se debe enviar una accion');
	echo json_encode($resultado);
	exit();
}

$result = array('status'=>'', 'message' =>'', 'action' => $action);
$_product = new Product();
$_fitment = new Fitment();

switch ($action) {
	case 'getAll':
		$page = $_GET['page'];
		$result = $_product->getAll($page);
		break;
	case 'getProductAttributes':
		$post_id = $_GET['post_id'];
		$result = $_product->getProductZum($post_id);
		$result = $_product->getProductAttributesWithNames($result);
		break;
	case 'getCurrentProductsIds':
		$result = $_product->getCurrentProductsIds();
		break;
	case 'sync':
		$sku = $_POST['product_sku'];
		$id = $_POST['id'];
		$result = $_fitment->syncElement($id,$sku);
		//$result = array('post' => $_POST, 'get' => $_GET, 'sku' => $sku);
		break;
	case 'autoSync':
		//echo 'autoSync';
		$_product->update_missing_products();
		//echo 'after update_missing_products';
		$id = $_GET['post_id'];

		$product = $_product->getCurrentSkuByPostId($id);
		//echo json_encode($product);
		if(!$product->product_sku){
			$result['status'] = 'ERROR';
			$result['message'] = "we can't get the product sku";
		}else{
			$result = $_fitment->syncElement($product->id,$product->product_sku);
			//echo json_encode($result);
			$result = $_fitment->getList($id, 1);
		}
		//$result = array('post' => $_POST, 'get' => $_GET, 'sku' => $sku);
		break;
	case 'getParameters':
		$post_id = $_GET['post_id'];
		$make = $_GET['make'];
		$model = $_GET['model'];
		$trim = $_GET['trim'];
		$engine = $_GET['engine'];

		$result = $_fitment->getParameters($post_id, $make, $model, $trim, $engine);
		//$result = array('post' => $_POST, 'get' => $_GET, 'sku' => $sku);
		break;
	case 'checkCompatibility':
		$post_id = $_GET['post_id'];
		$year = $_GET['year'];
		$make = $_GET['make'];
		$model = $_GET['model'];
		$trim = $_GET['trim'];
		$engine = $_GET['engine'];

		$result = $_fitment->checkCompatibility($post_id, $year, $make, $model, $trim, $engine);
		//$result = array('post' => $_POST, 'get' => $_GET, 'sku' => $sku);
		break;
	case 'getList':
		$post_id = $_GET['post_id'];
		$page = $_GET['page'];
		$result = $_fitment->getList($post_id, $page);
		//$result = array('post' => $_POST, 'get' => $_GET, 'sku' => $sku);
		break;
	default:
		# code...
		break;
}
//$result = json_encode($_POST);
header('Content-Type: application/json');
echo json_encode($result);

?>